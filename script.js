function calcular() {
    var data = new Date()
    var ano = data.getFullYear()
    var fano = document.getElementById('txtano')
    var res = document.querySelector('div#res')
    if (fano.value.length == 0 || fano.value > ano) {
        window.alert('[ERRO] Verifique os dados e tente de novo!')
    } else {
        var fsex = window.document.getElementsByName('radsex')
        var idade = ano - Number(fano.value)
        var gen = ''
        var img = document.createElement('img')
        img.setAttribute('id', 'foto')
        if (fsex[0].checked) {
            gen = 'homem'
            if (idade >= 0 && idade < 10) {
                img.setAttribute('src', 'bebe_M.png')
            } else if (idade < 21) {
                img.setAttribute('src', 'jovem_M.png')
            } else if (idade < 50) {
                img.setAttribute('src', 'adulto_M.png')
            } else {
                img.setAttribute('src', 'idoso_M.png')
            }
        } else if (fsex[1].checked) {
            gen = 'mulher'
            if (idade >= 0 && idade < 10) {
                img.setAttribute('src', 'bebe_F.png')
            } else if (idade < 21) {
                img.setAttribute('src', 'jovem_F.png')
            } else if (idade < 50) {
                img.setAttribute('src', 'adulto_F.png')
            } else {
                img.setAttribute('src', 'idoso_F.png')
            }
        }
        res.style.textAlign = 'center'
        res.innerHTML = `<p>Detectamos ${gen} com ${idade} anos.</p>`
        res.appendChild(img)
    }
}